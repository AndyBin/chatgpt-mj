## info

ShitongxueAI Package Publish, Download [URL](https://gitee.com/rumosky/chatgpt-mj/releases)

Powered by [rumosky](https://rumosky.com)

#### version intro

The `.sig` file is a signature file commonly used to provide a digital signature for another file. The primary purpose of this type of file is to ensure the integrity of the file and verify its origin, ensuring that the file has not been tampered with and is indeed released by the claimed publisher or author.

> `tar.gz` or `zip` is a compression file format.

## Windows

`ShitongxueAI_xxx_x64_en-US.msi` _# Recommend_
`ShitongxueAI_xxx_x64_en-US.msi.zip`

`ShitongxueAI_xxx_x64-setup.exe`
`ShitongxueAI_xxx_x64-setup.nsis.zip`

## MacOS

`ShitongxueAI_xxx_universal.dmg` _# Recommend_
`ShitongxueAI_universal.app.tar.gz`

## Linux

`shitongxue-ai_xxx_amd64.AppImage` _# Recommend_
`shitongxue-ai_xxx_amd64.AppImage.tar.gz`

`shitongxue-ai_xxx_amd64.deb` _# Debian/Ubuntu_
